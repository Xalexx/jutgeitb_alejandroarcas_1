import java.util.Scanner

val sc = Scanner(System.`in`)

fun main(){

    val attempts = mutableListOf<Intent>()
    for (i in problems.indices){
        problems[i].buildProblem()
        do {
            println("\nQuieres resolver el problema?\n1 -> Si    2 -> No\n")
            var wantToPlay = sc.nextLine()
            when (wantToPlay) {
                "1" -> {
                    problems[i].playUser()
                    attempts.add(Intent(problems[i].intents))
                }
                "2" -> {
                    print("\n\n\nCarregant seguent problema...\n\n\n")
                    attempts.add(Intent(0))
                }
                else -> {
                    wantToPlay = "wrongAnswer"
                    println("\nIntroduce una respuesta valida")
                }
            }
        } while (wantToPlay == "wrongAnswer")
    }
    println("Has acabado todos los problemas, los intentos por problema son los siguientes:")
    for (i in attempts.indices){
        if (attempts[i].intents == 0) println("Problema ${i + 1}: No realizado")
        else println("Problema ${i + 1}: ${attempts[i].intents}")
    }
}


val problems = listOf(
    Problema(
        "Problema 1: Dobla un numero entero\nBienvenido al programa que te dobla un numero entero, a continuacion tendras un juego de pruebas para que\nveas el funcionamiento, finalmente se generará un numero aleatorio donde deberas introducir el resultado correspondiente.",
        listOf(JocProva("Entrada: 2", "Salida: 4"), JocProva("Entrada: 10", "Salida: 20"), JocProva("Entrada: 12", "Salida: 24")),
        listOf(JocProva("Entrada: 64", "128")),
        false,
        0
    ),
    Problema(
        "Problema 2: Suma dos numeros enteros\nBienvenido al programa que suma dos numeros enteros, a continuacion tendras un juego de pruebas para que\nveas el funcionamiento, finalmente se generarán dos numeros aleatorios donde deberas introducir la suma de ellos.",
        listOf(JocProva("Entrada: 4 4", "Salida: 8"), JocProva("Entrada: 10 12", "Salida: 24"), JocProva("Entrada: 12 8", "Salida: 20")),
        listOf(JocProva("Entrada: 65 34", "99")),
        false,
        0
    ),
    Problema(
        "Problema 3: Calcula el area de un rectangulo\nBienvenido al programa que calcula el area de un rectangulo, a continuacion tendras un juego de pruebas para que\nveas el funcionamiento, finalmente se debera introducir el resultado del area de dos enteros (altura, anchura).",
        listOf(JocProva("Entrada: 2 4", "Salida: 8"), JocProva("Entrada: 10 5", "Salida: 50"), JocProva("Entrada: 12 10", "Salida: 120")),
        listOf(JocProva("Entrada: 14 11", "154")),
        false,
        0
    ),
    Problema(
        "Problema 4: Es edad legal?\nBienvenido al programa que verifica si eres mayor de edad, a continuacion tendras un juego de pruebas para que\nveas el funcionamiento, finalmente se deberas introducir (true o false), siendo verdadero mayor de edad y falso menor.",
        listOf(JocProva("Entrada: 4", "Salida: false"), JocProva("Entrada: 19", "Salida: true"), JocProva("Entrada: 87", "Salida: true")),
        listOf(JocProva("Entrada: 18", "true")),
        false,
        0
    ),
    Problema(
        "Problema 5: Es mayor que...\nBienvenido al programa que verifica si un numero es mayor que otro, a continuacion tendras un juego de pruebas para que\nveas el funcionamiento, finalmente deberas introducir (true o false), en caso de que el primer numero sea mayor que el segundo.",
        listOf(JocProva("Entrada: 1 9", "Salida: false"), JocProva("Entrada: 25 3", "Salida: true"), JocProva("Entrada: 12 13", "Salida: false")),
        listOf(JocProva("Entrada: 15 15", "false")),
        false,
        0
    ),
)