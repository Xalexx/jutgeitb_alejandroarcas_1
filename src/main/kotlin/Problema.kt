class Problema(
    private var enunciat: String,
    private var jocProvaPublic: List<JocProva>,
    private var jocProvaPrivat: List<JocProva>,
    private var resolt: Boolean,
    var intents: Int
) {

    fun buildProblem() {
        println("$enunciat\n")
        println("Ejemplos:\n")
        getInfoJocProva(jocProvaPublic)
    }

    fun playUser() {
        var userAnswer: String
        do {
            println("${jocProvaPrivat[0].input}    Salida: ...\nIntroduce una respuesta")
            userAnswer = sc.nextLine()
            intents++
            if (userAnswer == jocProvaPrivat[0].output) {
                println("Enhorabuena, has acertado la respuesta correcta!\n")
                resolt = true

            } else {
                println("Respuesta incorrecta.")
            }
        } while (!resolt)
    }

    private fun getInfoJocProva(public: List<JocProva>){
        for (i in public.indices){
            print("${public[i].input}    ${public[i].output}\n")
        }
    }
}